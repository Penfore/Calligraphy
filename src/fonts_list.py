# fonts_list.py
#
# Copyright 2023 Calligraphy Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

FONTS_LIST = [
    "3-d",
    "5lineoblique",
    "acrobatic",
    "alligator",
    "alligator2",
    "avatar",
    "banner3-D",
    "bell",
    "big",
    "block",
    "chunky",
    "coinstak",
    "cosmic",
    "crawford",
    "cyberlarge",
    "defleppard",
    "dotmatrix",
    "epic",
    "fraktur",
    "fuzzy",
    "goofy",
    "gothic",
    "graffiti",
    "hollywood",
    "invita",
    "isometric1",
    "isometric3",
    "jazmine",
    "kban",
    "letters",
    "marquee",
    "mini",
    "nancyj",
    "nvscript",
    "poison",
    "puffy",
    "relief",
    "roman",
    "script",
    "serifcap",
    "shimrod",
    "small",
    "speed",
    "starwars",
    "thin",
    "tombstone",
    "univers",
    "usaflag",
]
